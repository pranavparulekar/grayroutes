###################
Site URL 
###################

Filter Page :
    
http://<domain>/search

API URL:
http://<domain>/city

*******************
Database Setup 
*******************

Execute below query sequentially.

create table grayroutes.cities_list(
	city_name varchar(30) NOT NULL, 
	area float,
	population varchar(30),
	height int,
	population_density double,
	births_per_woman_per_year double,
	growth_rate double
)ENGINE=MyISAM;

CREATE UNIQUE INDEX uni_idx_city
ON grayroutes.cities_list (`city_name`);

CREATE INDEX idx
ON grayroutes.cities_list (`area`, `height`, `births_per_woman_per_year`, `growth_rate`);

*******************
Import Data
*******************

LOAD DATA INFILE 'path_to_file/Cities_List.csv' 
INTO TABLE grayroutes.Cities_List
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(`city_name`,`area`,`population`, `height`,`population_density` ,`births_per_woman_per_year`, `growth_rate`);

Note : "Cities_List.csv" csv file is available in root folder.

*******************
Source Code Setup 
*******************

1. Change base URL 
   File Path : application/config/config.php
   Line :
       $config['base_url'] = 'your_domain';


