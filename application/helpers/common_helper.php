<?php

if (!function_exists('send_response')) {

    function send_response($data) {
        header('content-type:text/json');
        echo json_encode($data);
    }

}