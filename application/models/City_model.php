<?php

class City_model extends CI_Model {

    private $condArr = [];

    public function getList($data) {
        $inputArr = [
            ['births_per_woman_per_year', 'birth_min', 'birth_max'],
            ['population', 'population_min', 'population_max'],
            ['height', 'height_min', 'height_max'],
            ['area', 'area_min', 'area_max'],
        ];
        $qry = "SELECT * FROM cities_list ";

        $this->prepareRangeCondition($data, $inputArr);

        if (!empty($this->condArr)) {
            $qry .= " WHERE " . join(' AND ', $this->condArr);
        }
        
        return $this->db->query($qry)->result_array();
    }

    private function prepareRangeCondition($data, $inputArr) {
        $cond = '';
        
        foreach ($inputArr as $input) {
            list($dbField, $inputMinValField, $inputMaxValField) = $input;
            
            if (isset($data[$inputMinValField]) && isset($data[$inputMaxValField])) {
                $cond = "$dbField BETWEEN " . $data[$inputMinValField] . " AND " . $data[$inputMaxValField];
            } elseif (isset($data[$inputMinValField])) {
                $cond = "$dbField >= " . $data[$inputMinValField];
            } elseif (isset($data[$inputMaxValField])) {
                $cond = "$dbField <= " . $data[$inputMaxValField];
            } else {
                $cond = '';
            }
            
            if (!empty($cond)) {
                $this->condArr[] = $cond;
            }
        }
    }

}

?>