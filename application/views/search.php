<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    var rid = 0;
    var apiUrl = <?php echo "'" . base_url() . "city'"; ?>;

    $.get(apiUrl, [], function (res) {
        if (res.status == 200) {
            $('#city_list').html(res.data);
        }
    });

    $(document).ready(function () {
        $('input').on('keyup', function () {
            var formData = {};
            $('input').each(function () {
                if ($(this).val().trim().length != 0) {
                    formData[$(this).attr('name')] = $(this).val().trim();
                }
            });

            rid++;
            formData['rid'] = rid

            $.get(apiUrl, formData, function (res) {
                if (res.status == 200 && res.rid == rid) {
                    console.log(res.data);
                    $('#city_list').html(res.data);
                }
            });
        });


    });
</script>
<html>
    <body>
        <form id="searchfrm">
            Birth : <input name="birth_min" type="number" /> - <input name="birth_max" type="number"/><br/>
            Area : <input name="area_min" type="number"/> - <input name="area_max" type="number"/><br/>
            Height : <input name="height_min" type="number"/> - <input name="height_max" type="number"/><br/>
            Growth : <input name="growth_min" type="number"/> - <input name="growth_max" type="number"/><br/>
            <div id="city_list">
            </div>
        </form>
    </body>
</html>