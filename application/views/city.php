<div>
    <?php if(!empty($data)) : ?>
    <table style='border: 1px solid;border-collapse: collapse;'>
        <tr>
            <th>City name</th>
            <th>Area</th>
            <th>Population </th>
            <th>Height </th>
            <th>Population density </th>
            <th>Births per woman per year </th>
            <th>Growth Rate</th>
        </tr>
        <?php foreach($data as $row) : ?>
        <tr>
            <td><?php echo $row['city_name']; ?></td>
            <td><?php echo $row['area']; ?></td>
            <td><?php echo $row['population']; ?></td>
            <td><?php echo $row['height']; ?></td>
            <td><?php echo $row['population_density']; ?></td>
            <td><?php echo $row['births_per_woman_per_year']; ?></td>
            <td><?php echo $row['growth_rate']; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>