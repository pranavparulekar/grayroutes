<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {

    public function index() {
        $this->load->helper('common_helper');
        $this->filter_input();

        $this->load->model('city_model');
        $res = $this->city_model->getList($_GET);
        $resid = isset($_GET['rid']) ? $_GET['rid'] : 0;

        if (!empty($res)) {
            send_response([
                'status' => 200,
                'message' => 'success',
                'rid' => $resid,
                'data' => $this->load->view('city', ['data' => $res], TRUE)
            ]);
        } else {
            send_response([
                'status' => 200,
                'message' => 'no_data',
                'rid' => $resid,
                'data' => '<strong>No data found</strong>'
            ]);
        }
    }

    public function filter_input() {
        foreach ($_GET as $key => $val) {
            $val = trim(filter_var($_GET[$key], FILTER_VALIDATE_FLOAT | FILTER_VALIDATE_INT));

            if (!empty($val)) {
                $_GET[$key] = (float) $val;
            } else {
                unset($_GET[$key]);
            }
        }
    }

}
